package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.xml.ws.WebServiceException;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointSrv =
            new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint =
            sessionEndpointSrv.getSessionEndpointPort();

    @Nullable
    private Session session;

    @Test
    @Category(UnitCategory.class)
    public void openSession() {
        session = sessionEndpoint.openSession("admin", "admin");
        Assert.assertNotNull(session);
    }

    @Test(expected = WebServiceException.class)
    @Category(UnitCategory.class)
    public void openSessionAccessDenied() {
        session = sessionEndpoint.openSession("admin", "admin1");
        Assert.assertNotNull(session);
    }


}
