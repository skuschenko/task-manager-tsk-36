package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;

import java.util.UUID;

public class TaskEndpointTest {

    @NotNull
    private static final String COMPLETE = "complete";

    @NotNull
    private static final String IN_PROGRESS = "in progress";

    @NotNull
    private static final SessionEndpointService sessionEndpointSrv =
            new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointSrv.getSessionEndpointPort();

    @NotNull
    private static final TaskEndpointService taskEndpointSrv =
            new TaskEndpointService();

    @NotNull
    private static final TaskEndpoint taskEndpoint =
            taskEndpointSrv.getTaskEndpointPort();

    @Nullable
    private static Session session;

    @Nullable
    private static Task task;

    @AfterClass
    public static void afterClassFunction() {
        sessionEndpoint.closeSession(session);
        taskEndpoint.clearTask(session);
    }

    @BeforeClass
    public static void beforeClassFunction() {
        session = sessionEndpoint.openSession("admin", "admin");
    }

    @Before
    public void beforeFunction() {
        task = taskEndpoint.addTask(session, UUID.randomUUID() + "name1", "des1");
    }

    @Test
    @Category(UnitCategory.class)
    public void testChangeStatusById() {
        Assert.assertNotNull(task);
        taskEndpoint.changeTaskStatusById(
                session, task.getId(), "COMPLETE"
        );
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(COMPLETE, taskFind.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void testChangeStatusByName() {
        taskEndpoint.changeTaskStatusByName(
                session, task.getName(), "COMPLETE"
        );
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskByName(
                        session, task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                COMPLETE, taskFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testClear() {
        taskEndpoint.clearTask(session);
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testCompleteById() {
        taskEndpoint.completeTaskById(session, task.getId());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                COMPLETE, taskFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testCompleteByName() {
        Assert.assertNotNull(task.getName());
        taskEndpoint.completeTaskByName(session, task.getName());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskByName(
                        session, task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                COMPLETE, taskFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() {
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneByName() {
        @Nullable final Task taskFind =
                taskEndpoint.findTaskByName(
                        session, task.getName()
                );
        Assert.assertNotNull(taskFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneById() {
        taskEndpoint.removeTaskById(session, task.getId());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByIndex() {
        taskEndpoint.removeTaskByIndex(session, 0);
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByName() {
        taskEndpoint.removeTaskByName(
                session, task.getName()
        );
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testStartById() {
        taskEndpoint.startTaskById(session, task.getId());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                IN_PROGRESS, taskFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testStartByName() {
        taskEndpoint.startTaskByName(session, task.getName());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskByName(
                        session, task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                IN_PROGRESS, taskFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testUpdateOneById() {
        taskEndpoint.updateTaskById(
                session, task.getId(), "name2", "des2"
        );
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final Task taskFind =
                taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

}
