package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;

import javax.xml.ws.WebServiceException;
import java.util.UUID;

public class UserEndpointTest {

    @NotNull
    private static final SessionEndpointService sessionEndpointSrv =
            new SessionEndpointService();
    @NotNull
    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointSrv.getSessionEndpointPort();
    @NotNull
    private static final UserEndpointService userEndpointService =
            new UserEndpointService();
    @NotNull
    private static final UserEndpoint userEndpoint =
            userEndpointService.getUserEndpointPort();
    @Nullable
    private static Session session;
    @Nullable
    private static User user;

    @AfterClass
    public static void afterClassFunction() {
        sessionEndpoint.closeSession(session);
    }

    @BeforeClass
    public static void beforeClassFunction() {
        session = sessionEndpoint.openSession("admin", "admin");
    }

    @Before
    public void beforeFunction() {
        user = userEndpoint.createUserWithRole(
                UUID.randomUUID() + "name1", "pass", "ADMIN"
        );
    }

    @Test(expected = WebServiceException.class)
    @Category(UnitCategory.class)
    public void testCreateWithEmail() {
        @NotNull final User user =
                userEndpoint.createUserWithEmail(
                        "user1", "user1", "user1@user1.ru"
                );
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals("user1@user1.ru", user.getEmail());
    }

    @Test
    @Category(UnitCategory.class)
    public void testCreateWithRole() {
        @NotNull final User user =
                userEndpoint.createUserWithRole("user1", "user1", "USER");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals("User", user.getRole());
    }

    @Test(expected = WebServiceException.class)
    @Category(UnitCategory.class)
    public void testFindByEmail() {
        @Nullable final User userFind =
                userEndpoint.findUserByEmail(user.getEmail());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindByLogin() {
        @Nullable final User userFind =
                userEndpoint.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void testLockUserByLogin() {
        userEndpoint.lockUserByLogin(session, user.getLogin());
        @Nullable final User userFind =
                userEndpoint.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userFind);
        Assert.assertTrue(userFind.isLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveByLogin() {
        userEndpoint.removeUserByLogin(session, user.getLogin());
        @Nullable final User userFind =
                userEndpoint.findUserByLogin(user.getLogin());
        Assert.assertNull(userFind);
        ;
    }

    @Test
    @Category(UnitCategory.class)
    public void testUnlockUserByLogin() {
        userEndpoint.unlockUserByLogin(session, user.getLogin());
        @Nullable final User userFind =
                userEndpoint.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userFind);
        Assert.assertFalse(userFind.isLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void testUpdateUser() {
        userEndpoint.updateUser(
                user.getId(), "FirstName", "LastName", "MiddleName"
        );
        @Nullable final User userFind =
                userEndpoint.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userFind);
        Assert.assertEquals("FirstName", userFind.getFirstName());
        Assert.assertEquals("MiddleName", userFind.getMiddleName());
        Assert.assertEquals("LastName", userFind.getLastName());
    }

}
