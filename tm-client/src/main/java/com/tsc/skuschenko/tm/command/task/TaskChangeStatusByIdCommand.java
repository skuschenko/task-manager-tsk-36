package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "change task by id";

    @NotNull
    private static final String NAME = "task-change-status-by-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);


        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String valueId = TerminalUtil.nextLine();
        @Nullable Task task = serviceLocator.getTaskEndpoint()
                .findTaskById(session, valueId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task = serviceLocator.getTaskEndpoint().changeTaskStatusById(
                session, valueId, readTaskStatus()
        );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
