package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable
    List<Task> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    );

}
