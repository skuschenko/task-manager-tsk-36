package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.model.Session;

public final class SessionRepository extends AbstractRepository<Session>
        implements ISessionRepository {

}
