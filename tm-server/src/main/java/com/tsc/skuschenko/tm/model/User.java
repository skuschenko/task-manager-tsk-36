package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    private boolean isLocked = false;

    @Nullable
    private String lastName;

    @Nullable
    private String login;

    @Nullable
    private String middleName;

    @Nullable
    private String passwordHash;

    @Nullable
    private String role = Role.USER.getDisplayName();

}
