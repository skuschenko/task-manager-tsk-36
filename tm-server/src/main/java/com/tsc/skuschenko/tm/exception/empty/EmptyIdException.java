package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

    public EmptyIdException(final String idName) {
        super("Error! '" + idName.toUpperCase() + "' is empty...");
    }

}
