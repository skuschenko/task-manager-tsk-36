package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class TaskRepositoryTest {

    @Test
    public void testChangeStatusById() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus("status1");
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals("status1", taskFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus("status1");
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals("status1", taskFind.getStatus());
    }

    @Test
    public void testClear() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        taskRepository.clear();
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    public void testCompleteById() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final Task task = testTaskModel();
        task.setStatus(Status.COMPLETE.getDisplayName());
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        Assert.assertNotNull(task.getName());
        @Nullable final Task taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final Task task = testTaskModel();
        testRepository(task);
    }


    @Test
    public void testFindOneById() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        @Nullable final Task taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByName() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        @Nullable final Task taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        taskRepository.removeById(task.getId());
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    public void testRemoveOneByIndex() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        taskRepository.removeOneByIndex(task.getUserId(), 0);
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    public void testRemoveOneByName() {
        @Nullable final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        taskRepository.removeOneByName(
                task.getUserId(), task.getName()
        );
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNull(taskFind);
    }

    @NotNull
    private ITaskRepository testRepository(@NotNull final Task task) {
        @NotNull final ITaskRepository taskRepository =
                new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(task);
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        @Nullable final Task taskById =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        return taskRepository;
    }

    @Test
    public void testStartById() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        Assert.assertNotNull(task.getStatus());
        @Nullable final Task taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @NotNull
    private Task testTaskModel() {
        @Nullable final Task task = new Task();
        task.setUserId("Id1");
        task.setName("name1");
        task.setDescription("des1");
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getUserId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("name1", task.getName());
        return task;
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setName("name2");
        task.setDescription("des2");
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final Task taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }


    @Test
    public void testUpdateOneByIndex() {
        @NotNull final Task task = testTaskModel();
        @NotNull final ITaskRepository
                taskRepository = testRepository(task);
        task.setName("name2");
        task.setDescription("des2");
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final Task taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

}
