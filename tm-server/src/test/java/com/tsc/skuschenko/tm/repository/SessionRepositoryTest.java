package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.model.Session;
import com.tsc.skuschenko.tm.util.HashUtil;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class SessionRepositoryTest {

    @Test
    public void testClear() {
        @Nullable final Session session = testSessionModel();
        @NotNull final ISessionRepository
                sessionRepository = testRepository(session);
        sessionRepository.clear();
        @Nullable final Session sessionFind =
                sessionRepository.findById(session.getId());
        Assert.assertNull(sessionFind);
    }

    @Test
    public void testCreate() {
        @NotNull final Session session = testSessionModel();
        testRepository(session);
    }

    @Test
    public void testFindOneById() {
        @Nullable final Session session = testSessionModel();
        @NotNull final ISessionRepository
                sessionRepository = testRepository(session);
        @Nullable final Session sessionFind =
                sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final Session session = testSessionModel();
        @NotNull final ISessionRepository
                sessionRepository = testRepository(session);
        sessionRepository.removeById(session.getId());
        @Nullable final Session sessionFind =
                sessionRepository.findById(session.getId());
        Assert.assertNull(sessionFind);
    }

    @NotNull
    private ISessionRepository testRepository(@NotNull final Session session) {
        @NotNull final ISessionRepository sessionRepository =
                new SessionRepository();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(session);
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        @Nullable final Session sessionById =
                sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        return sessionRepository;
    }

    @NotNull
    private Session testSessionModel() {
        @Nullable final Session session = new Session();
        session.setUserId("UId1");
        session.setId("Id1");
        session.setTimestamp(System.currentTimeMillis());
        String signature = SignatureUtil.sign(session,"password",454);
        session.setSignature(signature);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("UId1", session.getUserId());
        Assert.assertEquals("Id1", session.getId());
        Assert.assertEquals(
                signature,
                session.getSignature()
        );
        return session;
    }

}
