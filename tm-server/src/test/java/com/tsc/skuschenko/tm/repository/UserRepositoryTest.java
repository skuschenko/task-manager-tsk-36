package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class UserRepositoryTest {

    @Test
    public void testClear() {
        @Nullable final User user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        userRepository.clear();
        @Nullable final User userFind =
                userRepository.findById(user.getId());
        Assert.assertNull(userFind);
    }

    @Test
    public void testCreate() {
        @NotNull final User user = testUserModel();
        testRepository(user);
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        @Nullable final User userFind =
                userRepository.findByEmail(user.getEmail());
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        @Nullable final User userFind =
                userRepository.findByLogin(user.getLogin());
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testFindOneById() {
        @Nullable final User user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        @Nullable final User userFind =
                userRepository.findById(user.getId());
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final User user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        userRepository.removeByLogin(user.getLogin());
        @Nullable final User userFind =
                userRepository.findById(user.getId());
        Assert.assertNull(userFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final User user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        userRepository.removeById(user.getId());
        @Nullable final User userFind =
                userRepository.findById(user.getId());
        Assert.assertNull(userFind);
    }

    @NotNull
    private IUserRepository testRepository(@NotNull final User user) {
        @NotNull final IUserRepository userRepository =
                new UserRepository();
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(user);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        @Nullable final User userById =
                userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        return userRepository;
    }

    @NotNull
    private User testUserModel() {
        @Nullable final User user = new User();
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setEmail("Email@Email.ru");
        user.setLastName("LastName");
        user.setLogin("Login");
        user.setPasswordHash(HashUtil.salt("secret", 35484, "password"));
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getLogin());
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("FirstName", user.getFirstName());
        Assert.assertEquals("MiddleName", user.getMiddleName());
        Assert.assertEquals("LastName", user.getLastName());
        Assert.assertEquals("Login", user.getLogin());
        Assert.assertEquals("Email@Email.ru", user.getEmail());
        Assert.assertEquals(
                HashUtil.salt("secret", 35484, "password"),
                user.getPasswordHash()
        );
        return user;
    }

}
